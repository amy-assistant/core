CREATE GROUP plugin;
GRANT CONNECT ON DATABASE amy TO plugin;
GRANT USAGE ON SCHEMA public TO plugin;


CREATE TABLE account (
  id serial PRIMARY KEY,
  username VARCHAR(255) UNIQUE NOT NULL,
  email VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  first_name VARCHAR(255),
  last_name VARCHAR(255)
);


CREATE TABLE platform (
  id serial PRIMARY KEY,
  name VARCHAR(255) UNIQUE NOT NULL,
  enabled BOOLEAN DEFAULT TRUE NOT NULL,
  workers INT DEFAULT 1 NOT NULL,
  image VARCHAR(255) NOT NULL
);

CREATE TABLE account_platform (
  id serial PRIMARY KEY,
  account_id INT NOT NULL,
  platform_id INT NOT NULL,
  username VARCHAR(255),
  password VARCHAR(255),
  enabled BOOLEAN DEFAULT TRUE NOT NULL,
  session TEXT,
  CONSTRAINT account_platform_account_id_fk FOREIGN KEY (account_id) REFERENCES account (id),
  CONSTRAINT account_platform_platform_id_fk FOREIGN KEY (platform_id) REFERENCES platform (id)
);


CREATE OR REPLACE FUNCTION trigger_create_platform() RETURNS trigger AS $$
BEGIN
  EXECUTE 'CREATE USER ' || new."name" || ' WITH PASSWORD ''plugin'' IN GROUP plugin';
   EXECUTE 'CREATE OR REPLACE VIEW ' || new."name" || ' AS SELECT * FROM account_platform WHERE platform_id = ' || new."id" || ' WITH CHECK OPTION';
   EXECUTE 'GRANT SELECT ON ' || new."name" || ' TO ' || new."name";
   EXECUTE 'GRANT UPDATE ON ' || new."name" || ' TO ' || new."name";
  RETURN new;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER platform_trigger_create
AFTER INSERT ON platform
FOR EACH ROW
EXECUTE PROCEDURE trigger_create_platform();

CREATE OR REPLACE FUNCTION trigger_drop_platform() RETURNS trigger AS $$
BEGIN
  EXECUTE 'DROP USER ' || old."name";
  EXECUTE 'DROP VIEW ' || old."name";
  RETURN new;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER platform_trigger_drop
AFTER DELETE ON platform
FOR EACH ROW
EXECUTE PROCEDURE trigger_drop_platform();


CREATE OR REPLACE FUNCTION trigger_create_account_platform() RETURNS trigger AS $$
BEGIN
  PERFORM pg_notify((SELECT name FROM platform WHERE id = new.platform_id)::text, row_to_json(new) || ';INSERT');
  RETURN new;
END;
$$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE TRIGGER account_platform_trigger_create
AFTER INSERT ON account_platform
FOR EACH ROW
EXECUTE PROCEDURE trigger_create_account_platform();

CREATE OR REPLACE FUNCTION trigger_update_account_platform() RETURNS trigger AS $$
BEGIN
  PERFORM pg_notify((SELECT name FROM platform WHERE id = new.platform_id)::text, row_to_json(new) || ';UPDATE');
  RETURN new;
END;
$$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE TRIGGER account_platform_trigger_change
AFTER UPDATE ON account_platform
FOR EACH ROW
EXECUTE PROCEDURE trigger_update_account_platform();

CREATE OR REPLACE FUNCTION trigger_drop_account_platform() RETURNS trigger AS $$
BEGIN
  PERFORM pg_notify((SELECT name FROM platform WHERE id = old.platform_id)::text, row_to_json(old) || ';DELETE');
  RETURN new;
END;
$$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE TRIGGER account_platform_trigger_drop
AFTER DELETE ON account_platform
FOR EACH ROW
EXECUTE PROCEDURE trigger_drop_account_platform();

