FROM registry.gitlab.com/amy-assistant/core/base

ENV PRODUCTION=1

ENV PORT=80
ENV HOST=""
ENV AMY="amy"
ENV AMY_DB="amy"
ENV AMY_DB_USER="amy"
ENV AMY_DB_PASSWORD="amy"


EXPOSE ${PORT}


RUN apk add docker
RUN pip install docker

COPY . .

CMD [ "python","sry/main.py" ]